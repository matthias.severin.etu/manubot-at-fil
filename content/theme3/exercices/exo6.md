---
title: Exo 6
subtitle: Documents et calculatrices non autorisés à part le memento
author: Première année des licences MIASHS-PEIP-SESI, Univ. Lille
date: 06/01/2022
lang: fr-FR
geometry:
- a4paper
- margin=1.5cm
header-includes: |
  \usepackage{multicol}
...

**Reportez le numéro de votre groupe sur votre copie (-1 point si pas fait).** 
Ce sujet comprend des exercices indépendants.
Au sein d'un même exercice, il est demandé d'utiliser les fonctions
écrites dans les questions précédentes, lorsque c'est pertinent.
Toute fonction doit être réalisée en Python, 
et vous devez en **préciser les contraintes d'utilisation (1 point)**.
Par contre, la documentation n’est pas demandée
sauf si la question le précise explicitement.

Exercice 1 : _Compréhension de code_
======================================

<!-- question proposée par Pierre Allegraud -->

Question 1
: Pour chacune des séquences d'instructions suivantes, donner la valeur 
    et le type associés aux variables `a` et `n` en fin de séquence. 
    De plus, si vous pensez que le code produit une erreur, écrire "erreur" et justifier. 

::: fourcolumns
1. ```{.py out=code}
a = 2021
n = a
a = a + 1
```
<!--
`a` et `n` entiers de valeur 2022 et 2021 resp.
-->

2. ```{.py out=code}
a = [2, 0, 2]
n = a
a.append(2)
```
<!--
`a` et `n` liste d'entiers de valeur `[2, 0, 2, 2]`.
-->

3. ```{.py out=code}
a = [2, 0] * 2
n = a
a[3] = 2
```
<!--
`a` et `n` liste d'entiers de valeur `[2, 0, 2, 2]`.
-->

4. ```{.py out=code}
a = [2, 0, 2]
n = a
a = a + 2
```
<!--
`a` et `n` liste d'entiers de valeur `[2, 0, 2]`.
La dernière instruction provoque une erreur.
Plus précisément l'évaluation de l'expression `a+2` provoque une erreur,
on ne peut additionner une liste et un entier.
-->
:::

<!-- question proposée par Laurent Noé -->

Question 2
: Donner les valeurs prises par les variables `l`, `c` et `u` avant la boucle, 
  puis donner les valeurs des expressions `j`, `l[j]`, `c` et `u` 
  à la fin de chaque tour de boucle, lors de l'exécution du code ci-dessous. 

    ```{.python out=CODE}
    l = list(range(2,20,2))
    c = 0
    u = 0
    for j in range(len(l)):
        if l[j] % 3 == 0:
            c = c + 1
            u = u + j
    ```

<!-- 
- avant la boucle : l vaut [2, 4, 6, 8, 10, 12, 14, 16, 18], c et u valent 0
- à la fin de chaque tour de boucle :
j |l[j]| c |  u
--+----+---+----
0 |  2 | 0 |  0
1 |  4 | 0 |  0
2 |  6 | 1 |  2
3 |  8 | 1 |  2
4 | 10 | 1 |  2
5 | 12 | 2 |  7
6 | 14 | 2 |  7
7 | 16 | 2 |  7
8 | 18 | 3 | 15
-->


Exercice 2 : _Réécriture_
============================

<!-- exercice écrit par Maude sur une suggestion de Léopold qui a transféré un article publié dans le numéro 529 de Pour la Science ayant pour titre “La tenace conjecture de Syracuse”
et pour auteur Jean-Paul Delahaye --> 

Question 1
: En donnant sa **documentation complète**, écrire une fonction `reecriture_lettre()` qui prend en paramètre une lettre qui ne peut être que `'a'`, `'b'` ou `'c'` et renvoie la transformation de cette lettre selon les 3 règles :
  `'a'` $\to$ `'bc'`, `'b'` $\to$ `'a'`, `'c'` $\to$ `'aaa'`. 
  Les trois utilisations possibles sont :

    ```{.python out=NONE}
    def reecriture_lettre(lettre):
        """
        paramètre : lettre : (str)
        CU : lettre peut uniquement avoir comme valeur soit 'a', soit 'b', soit 'c'
             cela sous-entend que len(lettre) == 1
        valeur renvoyée : (str) la réécriture de la lettre selon les règles énoncées.
        Exemples :
        >>> reecriture_lettre('a')
        'bc'
        >>> reecriture_lettre('b')
        'a'
        >>> reecriture_lettre('c')
        'aaa'
        """
        if lettre == 'a':
            reecriture = 'bc'
        elif lettre == 'b':
            reecriture ='a'
        else: 
            reecriture ='aaa'
        return reecriture 
    ```

    ::: threecolumns
    ```{.python out=SHELL}
    >>> reecriture_lettre('a')
    >>> reecriture_lettre('b')
    >>> reecriture_lettre('c')
    ```
    :::

Question 2
: Écrire une fonction appelée `reecriture_mot()` qui prend en paramètre 
  une chaîne de caractères non vide qui ne peut être composée que de `'a'`, `'b'` ou `'c'`
  et renvoie la transformation de cette chaîne selon le principe suivant :

    - on ajoute à la fin du mot les lettres données par la transformation de la première lettre du mot ;
    - on enlève deux lettres situées au début du mot.

    Par exemple, on détaille la transformation de la chaîne `'bca'`. 
    Sa première lettre est `'b'` et se transforme en `'a'`. 
    La lettre `'a'` est donc ajoutée à la fin de la chaîne et on obtient `'bcaa'`. 
    Puis les deux premières lettres de cette nouvelle chaîne sont enlevées et on obtient `'aa'`.
    Voici des exemples d'exécution de cette fonction :

    ```{.python out=NONE}
    def reecriture_mot(mot):
        """CU : mot ne peut composée que de 'a', 'b' ou 'c'"""
        res = mot + reecriture_lettre(mot[0])
        return res[2:]
    ```

    ::: threecolumns
    ```{.python out=SHELL}
    >>> reecriture_mot('bca')
    >>> reecriture_mot('aa')
    >>> reecriture_mot('bc')
    ```
:::


Question 3
: Écrire une fonction appelée `nbre_reecritures()` qui prend en paramètre 
  une chaîne de caractères non vide qui ne peut être composée que de `'a'`, `'b'` ou `'c'`
  et renvoie le nombre de transformations, par le système précédent, 
  nécessaires pour atteindre la chaîne de caractères `'a'`.
  Par exemple :

    ```{.python out=NONE}
    def nbre_reecritures(mot):
        """CU : mot ne peut composée que de 'a', 'b' ou 'c'"""
        reecrit = mot
        n_iter = 0
        while reecrit != 'a':
            reecrit = reecriture_mot(reecrit)
            n_iter += 1
        return n_iter
    ```

    ```{.python out=SHELL}
    >>> nbre_reecritures('bca')
    ```

    Une conjecture (hypothèse) énonce que des transformations successives 
    par ce système aboutiront toujours à la chaîne `'a'`, quelle que 
    soit la chaîne de départ composée de `'a'`, `'b'` ou `'c'`. 
    Il n'y a donc pas de risque de boucle infinie.


Exercice 3 : _Texte geek_
=============================

<!-- exercice inspiré de questions proposées par Mirabelle --> 

Question 1
: Écrire une fonction `liste_de_chiffres()` qui prend en paramètre une chaîne de caractères
  et qui renvoie la liste des chiffres, de type entier, présents dans cette chaîne.
<!--
    Rappel : `isdigit()` renvoie vrai si le caractère auquel elle est appliquée
    est un chiffre et faux sinon, exemple :

    ::: twocolumns
    ```{.python out=SHELL}
    >>> 'a'.isdigit()
    >>> '1'.isdigit()
    ```
    :::
-->
    Exemples d'appels à `liste_de_chiffres()` :
    ```{.python out=NONE}
    def liste_de_chiffres(chaine):
        ''' CU : aucune'''
        l = []
        for c in chaine:
            if c.isdigit(): 
            # ou '0' <= c and c <= '9': 
            # ou c == '0' or c == '1' ...: 
            # ou c in 30123456789":
                l.append(int(c))
        return l
    ```

    ```{.python out=SHELL}
    >>> liste_de_chiffres('Il fait 7°C et le vent souffle à 10 km/h !')
    >>> liste_de_chiffres("Le 13 mars est le 72e jour d'une année non bissextile.") 
    >>> liste_de_chiffres("Serions-nous le 6 janvier 2022 ?") 
    ```

Question 2
: Écrire une fonction `multiplication()` qui prend en paramètre une liste 
non vide d'entiers et qui renvoie la multiplication des entiers qu'elle contient.
  Par exemple :

    ```{.python out=NONE}
    def multiplication(lint):
        ''' CU : liste ne contient que des entiers - marche aussi avec des floats'''
        mult = 1
        for i in lint:
            mult *= i
        return mult
    ```

    ::: twocolumns
    ```{.python out=SHELL}
    >>> multiplication([7, 1, 0])
    >>> multiplication([1, 3, 7, 2])
    ```
    :::

Question 3
: Écrire un prédicat appelé `contient_une_longueur42()` qui prend en paramètre 
  une liste de chaînes de caractères et qui renvoie vrai si la liste contient
  au moins une chaîne dont la longueur est égale à 42, faux sinon.
<!-- 
  Seules les réponses utilisant correctement une instruction `while`{.py} auront la totalité des points.
-->
  Par exemple :

    ```{.python out=NONE}
    def contient_une_longueur42_while(lstr):
        '''CU : none
        (on suppose avoir défini le paramètre comme une liste de str).
         sinon: 
           CU: lstr ne contient que des chaînes
         
        i = 0
        while i < len(lstr) and len(lstr[i]) != 42:
            i = i + 1
        return i != len(lstr)
            
    # alternative 
    def contient_une_longueur42_for(lstr):
        for chaine in lstr:
            if len(chaine) == 42:
                return True
        return False

    def contient_une_longueur42(lstr):
        return contient_une_longueur42_while(lstr)
    ```

    ```{.python out=SHELL}
    >>> contient_une_longueur42(['Météo.', 'Il fait 7°C et le vent souffle à 10 km/h !'])
    >>> contient_une_longueur42(['Le Guide du voyageur galactique.', 'Douglas Adams.'])
    ```

Etc. 
<!-- eof -->
