# [Insert Name]

**[Insert Name]** is an automatic workflow that use [pandoc](https://pandoc.org/) with the [pandoc-pyrun](https://pypi.org/project/pandoc-pyrun/) filter to convert your Markdown files into HTML, PDF and other format. After that they'll be posted online.  
You can simply work on your GIT, make change, commit them, and they'll be online a few minute later.  
This workflow can work on any Gitlab repository with **Gitlab CI and Gitlab Pages enable**.  

# Configuration  

Check the ![documentation here](doc/config.md) to properly configure the workflow on your repository before using it !  

# Usage  

## Content

Simply put all your Markdown files in the content directory.  
You can have a hierarchy with different directory in it.  
Every markdown file in the content directory will be converted into HTML and PDF format.  
Example:  

![](doc/images/hierarchy.png)

## Index / Uploading  

If you only have your Markdown files in the content directory, everything will be posted online.  
For example, if you in your content directory a file in dir1/dir2/file.md, you can access the HTML version with the URL : [base_pages_url]/dir1/dir2/file.html  

You can add an index.txt file at the root of the content directory, this will have for effect to generate an index page (URL: [base_pages_url]/) and upload online files that you specified in this index.txt file.

Here is one example of index.txt file and i'll explain how it works:  
```txt
# theme1 > Apprendre Python
## cours > Sujets de cours
### cours1 > Les bases py
### cours2 > Concepts avancés
## exercices > TP/TD
### exo1 > TD
### exo2 > TP
# theme2 > Apprendre Java
## cours > Sujets de cours
### cours3 > Les bases jv
# sujet > DS du 26/11/2021
```

The index page will looks like that:  

![](doc/images/index1.png)

And assuming we have this hierarchy of Markdown files in our content directory:  

![](doc/images/hierarchy.png)

Only those files will be posted online (in their HTML and optional formats versions):  
- theme1/cours/cours1.md
- theme1/cours/cours2.md
- theme1/exercices/exo1.md
- theme1/exercices/exo2.md
- theme2/cours/cours3.md
- sujet.md

So, how it works:  
You need to follow the hierarchy of your content directory, one line that start with an **#** represent one file or one directory.  
Each line as the same format: **[name of the dir/file in the content directory] > [name that will appear in the index]**  
**#** are really important, they represent the depth of the file/dir in the content directory, for example:  
- This means that you want to upload dir1/file1.md and file2.md:  
```txt
# dir1 > First section:
## file1 > First course
# file2 > Second course
```
- But this means that you want to upload dir1/file1.md and dir1/file2.md:  
```txt
# dir1 > First section:
## file1 > First course
## file2 > Second course
```

You can also add some css to this index page, just put an **index.css** file at the root of the content directory and it will be applyed to the index page.  
Directory sections are represented with **\<h4>** tags, file sections with **\<p>** tags and links with **\<a>** tags.  

## Index template  

You can also add a template for your index.  
You need to add a file in the **content directory** named **index_template.html**  
In this file you need to add a tag: **[index_location]**  
Example:  
```html
<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet" href="index.css"/>
<meta charset="utf-8"/>
<title>Index</title>
</head>
<body>
<h1> This index as been generated with the template</h1>
[index_location]
</body>
</html>
```
With this, the index will be placed where the tag is in your template.  

Dont forget to link the index.css file if you've added one in the content directory like it has been explain previously.  
```html
<link rel="stylesheet" href="index.css"/>
```

## Artifacts  

You can always get the generated files even if they arent posted online and get past versions.  
In your Gitlab project, go to **CI/CD > Pipelines**:  

![](doc/images/artifacts1.png)

Find the commit that you want to get generated files, click on the 3 dots at the right and here you can download 2 archives:

![](doc/images/artifacts2.png)

The generate_file archive contains all files in the content directory converted to all versions that you've specified in your Makefile.  

If you've commited on the main branch, you'll find the pages archive that contains everything that has be upload online: HTML, optional formats and index files.  

Else, if you havent commited on the main branch, you'll find the demo archive that contains everything that would be uploaded is this branch was the main one, so you can see your changes localy before pushing them to the main branch.  

## Uploading without Gitlab Pages  

The easiest way to use this workflow is to use Gitlab Pages for uploading files.  
But if you want, you can personnal server to upload them.  

I'll explain you how to do this with an FTP example.  

First, you need to create a script in the config directory that will upload the .public directory (it's generated with other script and contain all files to upload) to your server.  
FTP example, file config/ftp_upload.sh:  
```bash
HOST=webtp.fil.univ-lille1.fr
USER=severin
PASSWORD=xxx
# Path where files will be uploaded
UPLOAD_PATH=public_html/
# Local directory to upload, always upload .public directory for all content
LOCAL_PATH=../.public/*

ncftpput -avR -u $USER -p $PASSWORD $HOST $UPLOAD_PATH $LOCAL_PATH
```

Then, you need to change the .gitlab-ci.yml file.  
Remove the "pages" section and replace it by this:  
```yml
upload:
  image: python:latest
  stage: deploy
  script:
    # 1/ Install things needed to run your script
    - apt-get update
    - apt-get --assume-yes install ncftp
    # 2/ Standard script, dont need to change this
    - python config/generate_script_upload.py
    - ./upload_file.sh
    # 3/ Execute your script
    - ./ftp_upload.sh
  only:
    - main
```
Update this example with your personnal needs:  
- 1/ If their is something needed to run your script, you need to install it before executing your script  
- 2/ This is the standard script, dont change it  
- 3/ Execute your script  
