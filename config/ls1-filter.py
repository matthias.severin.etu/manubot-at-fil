#!/usr/bin/env python

"""
Pandoc filter to convert divs such as coup-d-oeil, memento, and even twocolumns,
to LaTeX environments in LaTeX output. 
"""

DIV2ENV = {'coup-d-oeil' : 'coupdoeil', 'memento': 'memento',
               'twocolumns' : 'multicols}{2', 'threecolumns' : 'multicols}{3',
               'fourcolumns' : 'multicols}{4'}

from pandocfilters import toJSONFilter, RawBlock, Div

def latex(x):
    return RawBlock('latex', x)

def ls1divs(key, value, format, meta):
    if key == 'Div':
        [[ident, classes, kvs], contents] = value
        if format == "latex":
            div = classes[0]
            if div in DIV2ENV :
                env = DIV2ENV[div]
                return([latex('\\begin{' + env + '}')] +
                        contents +
                        [latex('\\end{' + env.split('}')[0] + '}')])

if __name__ == "__main__":
    toJSONFilter(ls1divs)
