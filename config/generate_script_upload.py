import os
import stat

# The list of optionnal format
opt_format = ["pdf", "tex"]
# Name of the script file created
upload_file_name = "upload_file.sh"
# Path of the index.txt file
index_file_name = "content/index.txt"
# Path where the index html file will be created
index_html_name = ".public/index.html"
# Path of the html template
template_html_name = "content/index_template.html"

def build_header(script_file, css):
	"""
	Method create the header of the HTML file.
	If the css file exist, add it to the header and add to script file a line to copy it to the .public repository
	
	Parameters:
		script_file (file object): the script file object open in writing mode
		css (str): the name of the css file (assuming he's in content/ directory)
		
	Returns:
		header (str): the header of the index html file
	"""
	header = "<!DOCTYPE html>\n"
	header += "<html>\n"
	header += "<head>\n"
	# If the css file exist in content/ directory
	if os.path.isfile("content/" + css):
		script_file.write("cp content/" + css + " .public\n")
		header += "<link rel=\"stylesheet\" href=\"" + css + "\"/>\n"
	header += "<meta charset=\"utf-8\"/>\n"
	# You can change title of the index here
	header += "<title>Index</title>\n"
	header += "</head>\n"
	header += "<body>\n"
	return header
	
def build_index_script(index_txt, script_file):
	"""
	Method that create an array with every line of the index to insert in the html file and the script
	The script will copy the needed file from content/ to .public/ repository
	(Check the main readme to see what the index.txt file looks like)
	
	Parameters:
		index_txt (file object): the index txt file object open in reading mode
		script_file (file object): the script file object open in writing mode
		
	Returns:
		index (array): an array that contains every lines of the index in html format
	"""
	lines = index_txt.readlines();
	hierarchy = []
	index = []
	for line in lines:
		if (line.startswith("#")):
			tab_count = line.count("#")
			line = line[tab_count:]
			tab_count = tab_count - 1
			split_name = line.split(">")
			original_name = split_name[0].strip()
			wanted_name = split_name[1].strip()
			file_path = ""
			for i in range(tab_count):
				file_path = file_path + hierarchy[i] + "/"
			file_path = file_path + original_name
			if (os.path.isdir("output/" + file_path)):
				if (len(hierarchy) <= tab_count):
					hierarchy.append(original_name)
				else:
					hierarchy[tab_count] = original_name
				index_line = "<h4>" + tab_count * "&emsp;" + wanted_name + "</h4>\n"
				index.append(index_line)
			else:
				script.write("make .public/" + file_path + ".html\n")
				index_line = "<p>" + tab_count * "&emsp;"
				index_line = index_line + "<a href=./" + file_path + ".html>" + wanted_name + "</a>"
				nb_format = len(opt_format)
				if (nb_format > 0):
					index_line = index_line + " ("
					for i in range(nb_format):
						script.write("make .public/" + file_path + "." + opt_format[i] + "\n")
						index_line = index_line + "<a href=./" + file_path + "." + opt_format[i] + ">" + opt_format[i] + "</a>"
						if (i != nb_format - 1):
							index_line = index_line + ", "
					index_line = index_line + ")"
				index_line = index_line + "</p>\n"
				index.append(index_line)
	script.write("mv .public/output/* .public/\n")
	script.write("rm -d .public/output/")
	return index
	
def build_html_without_template(index_html, index_array, header):
	"""
	Method complete the html file of the index without template with the header and the index content
	
	Parameters:
		index_html (file object): the index html file object open in writing mode
		index_array (array): an array that contains every lines of the index in html format (created with build_index_script)
		header (str): the header of the html file
	"""
	index_html.write(header)
	for line in index_array:
		index_html.write(line)
	index_html.write("</body>\n</html>")
	
def build_html_with_template(html_template, index_html, index_array):
	"""
	Method copy in the index_html the content of html_template and insert the index in it
	The index will be added where their is the tag [index_location] in the html_template
	
	Parameters:
		html_template (file object): the html template, need to contain [index_location] else the index will be added at the end of the template file
		index_html (file object): the index html file object open in writing mode
		index_array (array): an array that contains every lines of the index in html format (created with build_index_script)
	"""
	data = html_template.readlines()
	tag_location = 0
	while ((tag_location < len(data)) and (not "[index_location]" in data[tag_location])):
		tag_location += 1
	new_data = data[:tag_location]
	for line in index_array:
		new_data.append(line)
	for i in range(tag_location + 1, len(data)):
		new_data.append(data[i])
	index_html.writelines(new_data)

###########################

# Create the script file and give it execution permission
script = open(upload_file_name, "w")
st = os.stat(upload_file_name)
os.chmod(upload_file_name, st.st_mode | stat.S_IEXEC)

# Create .public repository
os.mkdir("./.public/")

try:
	index = open(index_file_name, "r")
except FileNotFoundError:
	# If their is no index txt we just copy all content in .public
	script.write("cp -r output/* .public\n")
else:
	header = build_header(script, "index.css")
	index_array = build_index_script(index, script)
	# With template
	if os.path.isfile(template_html_name):
		build_html_with_template(open(template_html_name, "r"), open(index_html_name, "w"), index_array)
	# Without template
	else:
		build_html_without_template(open(index_html_name, "w"), index_array, header)
