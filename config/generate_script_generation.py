import glob
import os
import stat

# The name of the script file to create
generate_file = "generate_file.sh"

def create_dir(path):
	"""
	Create a directory and pass exception if directory already exist
	
	Parameters:
		path (str): the path of the directory to create
	"""
	try:
		os.mkdir(path)
	except OSError as error: 
		pass

create_dir("./output/")

# Create the script file and give it execution permission
script = open(generate_file, "w")
st = os.stat(generate_file)
os.chmod(generate_file, st.st_mode | stat.S_IEXEC)

# For each .md files in the content directory recursivly, use the makefile to convert the file into the output directory
for file in glob.iglob("content/" + '/**/*.md', recursive=True):
	path = "./output/"
	split = file.split("/")
	for i in range(1, len(split) - 1):
		path = os.path.join(path, split[i])
		create_dir(path)
	file_name = file[8:-3] # remove content/ and .md
	script.write("make output/" + file_name + "\n")
