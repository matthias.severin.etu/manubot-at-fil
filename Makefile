# config pandoc
PANDOC = pandoc
PANDOCOPT = -d config/ls1-dfault.yaml

# rules for file generation
output/%.pdf: content/%.md
	$(PANDOC) $(PANDOCOPT) -s -o $@ $<
output/%.html: content/%.md
	$(PANDOC) $(PANDOCOPT) -s -o $@ $<
output/%.tex: content/%.md
	$(PANDOC) $(PANDOCOPT) -s -o $@ $<

# rule used with generate_script.py, she need to make all format that you want to get with the pipeline and at least every format that you want to add in pages (html + optionnal)
output/%: content/%.md
	make $@.pdf
	make $@.html
	make $@.tex
	
# rules for uploading, copy files from output to .public keeping the hierarchy
.public/%: output/%
	cp --parent $< .public
