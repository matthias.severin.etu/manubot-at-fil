# Configuration  

## Image  

First, you need to configure the .gitlab-ci.yml file.  
To make the Gitlab CI work you need to use an image that has everything needed.  
You have 2 choices :  

- Use the standard **python image** and change the CI generate_file stage to install **pandoc, texlive, texlive-xetex, ghostscript and pandoc_pyrun** on this image at each commit.  
Example:  
```yml
generate_file:
  image: python:latest
  stage: generate_file
  script:
    - apt-get update
    - apt-get --assume-yes install pandoc # install pandoc, careful pandoc need to be at version 2.9+
    - apt-get --assume-yes install texlive # install texlive
    - apt-get --assume-yes install texlive-xetex # install texlive-xetex
    - apt-get --assume-yes install ghostscript # install ghostscript
    - python -m pip install --upgrade pip
    - pip install pandoc_pyrun # install pandoc_pyrun
    # standard execution
    - python config/generate_script_generation.py
    - ./generate_file.sh
  artifacts:
    paths:
      - output
```

- Use a **custom image** with everything already install on it, so you don't need to install them on each commit.  
Example:  
```yml
generate_file:
  image: python-manubot:latest # change this by the name of your custom image !
  stage: generate_file
  script:
    # standard execution
    - python config/generate_script_generation.py
    - ./generate_file.sh
  artifacts:
    paths:
      - output
```

Both have advantages, the custom images will be way faster cause everything is already in the image but you need to create it and update it when needed, for exemple, if a new version of pandoc_pyrun is out and you want to use it, you need to create a new image with this new version in it, whereas with the python image, you install it on each commit so you'll always install the last version available.  

## Main branch  

Also, in the "pages" stage, you can change the branch that you want to be your main branch, every commit on this branch will be posted online, and commit in other branch.  
At the end of the CI file, you'll find this:  
```yml
  only:
    - main
```
Just change "main" by the name of your main branch if it's different.  

And same for the "demo" stage, change "main" by the name of your main branch (need to be the same as before):  
```yml
  except:
    - main
```

## Optional formats uploaded  

You can choose wich format that you want to upload on pages, html is the base format but you can add more.  
You need to update the config/generate_script_upload.py file and change the opt_format array:  

Example 1: You want pdf and latex as optional formats:  
```python
opt_format = ["pdf", "tex"]
```
Example 2: You just want pdf as optional format:  
```python
opt_format = ["pdf"]
```
Example 3: You dont want optional format:  
```python
opt_format = []
```

## Makefile

The makefile is an important thing in your project, it's here that you'll specify how your Markdown files will be converted.  

In your Makefile you need 2 rules:  

- **output/%: content/%.md**  
This rules generate every format that you want to get for an .md file, but that doesnt mean that they'll be upload online (check before for optional formats uploaded)
You can generate html, pdf and latex format and only upload html and pdf.  
But this rule need to generate at least all your optional formats.  
Example:  
```make
# rules for file generation
output/%.pdf: content/%.md
	$(PANDOC) $(PANDOCOPT) -s -o $@ $<
output/%.html: content/%.md
	$(PANDOC) $(PANDOCOPT) -s -o $@ $<
output/%.tex: content/%.md
	$(PANDOC) $(PANDOCOPT) -s -o $@ $<

# rule used with generate_script.py, she need to make all format that you want to get with the pipeline and at least every format that you want to add in pages (html + optionnal)
output/%: content/%.md
	make $@.pdf
	make $@.html
	make $@.tex
```

- **.public/%: output/%**  
This rule dont need to be change unless you want to use something else than "cp"  
It just copy a file from output to .public keeping all his hierarchy (output included)  
```make
# rules for uploading, copy files from output to .public keeping the hierarchy
.public/%: output/%
	cp --parent $< .public
```
So if you have this hierarchy in your output directory :  

![](images/make1.png)

After doing *"make .public/theme1/cours/cours1.html"* and *"make .public/theme1/cours/cours1.pdf"* you should have something like this in your .public directory :  

![](images/make2.png)
